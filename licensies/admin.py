from django.contrib import admin

# Register your models here.
from licensies.models import Client, License


class LicenseAdmin(admin.ModelAdmin):
    list_display = ('license_id', 'license_owner', 'valid_from', 'valid_till')

admin.site.register(License, LicenseAdmin)
admin.site.register(Client)
