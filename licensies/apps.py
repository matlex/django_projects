from django.apps import AppConfig


class LicensiesConfig(AppConfig):
    name = 'licensies'
