from django.forms import ModelForm, TextInput, Select
from licensies.models import License
from bootstrap3_datetime.widgets import DateTimePicker


class LicenseForm(ModelForm):
    class Meta:
        model = License
        fields = ['license_id', 'license_password', 'license_owner', 'valid_from', 'valid_till']
        widgets = {
            'license_id': TextInput({'class': "form-control", 'required': True}),
            'license_password': TextInput({'class': "form-control", 'required': True}),
            'license_owner': Select({'class': "form-control", 'required': True}),
            'valid_from':  DateTimePicker(options={
                'format': "YYYY-MM-DD",
                'pickTime': False,
            }),
            'valid_till':  DateTimePicker(options={
                'format': "YYYY-MM-DD",
                'pickTime': False,
            })
        }
