# -*- coding: utf-8 -*-
from licensies.models import License, Client
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
import os

# For Class Based views
from django.views.generic import ListView, DeleteView
from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse_lazy

# Form for based forms
from forms import LicenseForm
from django.shortcuts import render
from django.http import HttpResponseRedirect

# For Bitbucket Hooks
from django.http import HttpResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
import git

from Eset_Project import settings


# Используем Class-Based представление на основе предсозданных из generic.
# В приципе удобно хоть и не обычно. http://djbook.ru/rel1.8/topics/class-based-views/generic-display.html
class LicensesList(ListView):
    model = License
    template_name = 'licensies/license.html'
    context_object_name = "licenses_list"
    paginate_by = 8
    queryset = License.objects.order_by('-pk')

    @method_decorator(login_required)  # Здесь потребуем просто быть залогиненым
    def dispatch(self, *args, **kwargs):
        return super(LicensesList, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        # Refreshing our license.txt file. Each time when accessing to a list of licenses page.
        create_license_file()
        return self.queryset


class LicenseDelete(DeleteView):
    model = License
    success_url = reverse_lazy('licmanager_home')
    template_name = 'licensies/delete_license.html'  # Если не создать этот шаблон вручную, то джанго создаст его сама.

    @method_decorator(staff_member_required)  # А здесь потребуем права у юзера чтоб были staff.
    def dispatch(self, *args, **kwargs):
        return super(LicenseDelete, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        # saving good result to a session
        request.session['license_deleted'] = True
        return self.delete(request, *args, **kwargs)


class ClientAdd(CreateView):
    model = Client
    fields = ['name']
    template_name = 'licensies/add_client.html'
    success_url = reverse_lazy('create_license_client')


# Используем обычные def-based представления
@login_required()
def license_create(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = LicenseForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            lic_obj = License(
                license_id=form.cleaned_data['license_id'],
                license_password=form.cleaned_data['license_password'],
                license_owner=form.cleaned_data['license_owner'],
                valid_from=form.cleaned_data['valid_from'],
                valid_till=form.cleaned_data['valid_till']
            )
            lic_obj.save()
            # saving good result to a session
            request.session['license_created'] = True
            # redirect to a new URL:
            return HttpResponseRedirect(request.META['HTTP_REFERER'])
        else:
            return render(request, 'licensies/create_license.html', {'form': form})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = LicenseForm()
        return render(request, 'licensies/create_license.html', {'form': form})


@login_required()
def license_create_modal(request):
    form = LicenseForm()
    return render(request, 'licensies/create_license_modal.html', {'form': form})


# Helper Functions
def create_license_file():
    """
    Save a file into test /home/matlex/Desktop
    """
    list_of_licenses = License.objects.order_by('-pk')
    try:
        with open(r"/etc/lighttpd/eset.users", "w") as f:
            for license_obj in list_of_licenses:
                f.write("{0}:{1}\n".format(license_obj.license_id, license_obj.license_password))
        print "License file saved successfully.", list_of_licenses
    except Exception as e:
        print e


@require_POST
@csrf_exempt
def hello(request):
    # Some changes
    header_signature = request.META.get('HTTP_X_HOOK_UUID')
    if not header_signature or header_signature != settings.BITBUCKET_HTTP_X_Hook_UUID:
        return HttpResponseForbidden('Permission denied.')

    g = git.cmd.Git()
    g.pull()
    print "Pulled from Bitbucket Success"
    return HttpResponse({'status': 'success'})
