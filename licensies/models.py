from __future__ import unicode_literals

from django.db import models


# Create your models here.
class Client(models.Model):
    name = models.CharField(max_length=400, blank=False, null=False, unique=True)

    def __unicode__(self):
        return self.name


class License(models.Model):
    license_id = models.CharField(max_length=20, blank=False, null=False)
    license_password = models.CharField(max_length=20, blank=False, null=False)
    license_owner = models.ForeignKey(Client)
    valid_from = models.DateField(blank=True, null=True)
    valid_till = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name = 'Eset Licensies'

    def __str__(self):
        return self.license_id
