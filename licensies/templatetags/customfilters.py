from django import template

register = template.Library()


@register.filter(name='addcss')
def addcss(field, css):
    class_old = field.field.widget.attrs.get('class', None)
    class_new = class_old + ' ' + css if class_old else css
    return field.as_widget(attrs={"class": class_new})


@register.filter(name='addrequired')
def addrequired(field, css):
    class_old = field.field.widget.attrs.get('class', None)
    class_new = class_old + ' ' + css if class_old else css
    return field.as_widget(attrs={"class": class_new, "required": True})


# Some helper
@register.filter(name='delete_session')
def delete_session(request):
    # delete the session value
    try:
        del request.session['license_created']
    except KeyError:
        pass
    try:
        del request.session['license_deleted']
    except KeyError:
        pass
