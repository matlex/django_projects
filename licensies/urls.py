"""Eset_Project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from .views import LicensesList, LicenseDelete, license_create, license_create_modal, ClientAdd, hello


urlpatterns = [
    url(r'^$', LicensesList.as_view(), name='licmanager_home'),
    url(r'^delete/(?P<pk>\d+)/$', LicenseDelete.as_view(), name="delete_license"),
    url(r'^create/$', license_create, name="create_license"),
    url(r'^create/modal/$', license_create_modal, name="create_license_modal"),
    url(r'^page/(?P<page>\d+)/$', LicensesList.as_view()),
    url(r'addclient/$', ClientAdd.as_view(), name="create_license_client"),

    # Bitbucket Hooks
    url(r'^api/hello/$', hello, name='hello'),
]
