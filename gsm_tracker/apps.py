from django.apps import AppConfig


class GsmTrackerConfig(AppConfig):
    name = 'gsm_tracker'
