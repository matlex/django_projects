# -*- coding: utf-8 -*-
"""
request H02 protocol example: "*HQ,353505511042795,V1,,V,,N,,E,0.00,0,,FFFFFFFF,191,309,7fd4,d612#"
For run and port listening use python manage.py start_gsm_tracker
"""

import json
import socket
import datetime
import SocketServer

from grab import Grab
from dateutil.tz import tzutc, tzlocal
from django.core.management.base import BaseCommand
from gsm_tracker.models import Coordinate, TrackedCoordinate

# Пока будем хранить данные имей и номера машины в этом словаре.
vehicle_gsm_details = {
    353505511042795: "B3232N"
}

# Local and UTC timezones issues: http://asvetlov.blogspot.com/2011/02/date-and-time.html
tzutc = tzutc()  # Will store in DB as UTC time +00:00
tzlocal = tzlocal()  # Will work in our APP


def parse_request(request_string):
    """
    Takes string from GSM tracker device like *HQ,353505511042795,V1,,V,,N,,E,0.00,0,,FFFFFFFF,191,309,7fd4,d612#
    and returns cellular base parameters. http://www.open-electronics.org/how-to-find-the-location-with-gsm-cells/
    """
    request_string = request_string.split(',')
    request_string = [i.strip() for i in request_string if len(i.strip()) > 0]

    imei = int(request_string[1])
    mcc = int(request_string[-4], 16)
    mnc = int(request_string[-3], 16)
    lac = int(request_string[-2], 16)
    cid = int(request_string[-1].strip()[:-1], 16)

    return [imei, mcc, mnc, lac, cid]


def track_device(mcc, mnc, lac, cid):
    google_api_url = 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDp314JBTVhzQR8XP-pR8GbrxyELGx91j0'
    g = Grab()
    params = json.dumps({
        "cellTowers": [
            {
                "cellId": cid,
                "locationAreaCode": lac,
                "mobileCountryCode": mcc,
                "mobileNetworkCode": mnc
            }]})
    g.setup(post=params)
    g.setup(headers={'Content-Type': 'application/json'})

    resp = g.go(google_api_url)
    dic = json.loads(resp.body)
    location = dic.get("location")
    return [location.get("lat"), location.get("lng")]


def save_to_db(vehicle_number, lat_lng):
    coords_obj = Coordinate.objects.get_or_create(geo_latitude=lat_lng[0], geo_longitude=lat_lng[1])[0]
    tracked_obj = TrackedCoordinate.objects.create(
        vehicle_number=vehicle_number,
        geo_coords=coords_obj
    )
    tracked_obj.save()
    print "Saved Successfully to DB."


previous_date = None  # For tracking date
previous_data = None


# Playing with Python's SocketServer
class MyTCPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        global previous_data, previous_date
        try:
            data = self.request.recv(1024).strip()
            print "{} wrote:".format(self.client_address[0])
            print data
            # just send back the same data, but upper-cased
            # self.request.sendall(self.data.upper())
            current_date = datetime.datetime.today().date()
            if data != previous_data:
                print "WORKING FIRST CONDITION"
                previous_data = data
                previous_date = current_date
                for imei in vehicle_gsm_details.iterkeys():
                    if str(imei) in data:
                        # print "We have request from Harrier GSM Tracker device."
                        imei, mcc, mnc, lac, cid = parse_request(data)
                        lat_lng = track_device(mcc, mnc, lac, cid)
                        utc_time = datetime.datetime.now(tz=tzutc)  # time in UTC
                        # local_time = utc_time.astimezone(tzlocal)  # UTC time interpreted as local
                        vehicle_number = vehicle_gsm_details.get(imei)
                        print "LAT&LNG:", lat_lng
                        print "UTC time:", utc_time
                        # print "Local time", local_time  # UTC time interpreted as local
                        print "Vehicle number:", vehicle_number
                        save_to_db(vehicle_number, lat_lng)
                print "Source Data:", data

            elif current_date != previous_date:
                print "WORKING SECOND CONDITION", current_date, previous_date
                print current_date == previous_date
                previous_data = data
                previous_date = current_date
                for imei in vehicle_gsm_details.iterkeys():
                    if str(imei) in data:
                        # print "We have request from Harrier GSM Tracker device."
                        imei, mcc, mnc, lac, cid = parse_request(data)
                        lat_lng = track_device(mcc, mnc, lac, cid)
                        utc_time = datetime.datetime.now(tz=tzutc)  # time in UTC
                        # local_time = utc_time.astimezone(tzlocal)  # UTC time interpreted as local
                        vehicle_number = vehicle_gsm_details.get(imei)
                        print "LAT&LNG:", lat_lng
                        print "UTC time:", utc_time
                        # print "Local time", local_time  # UTC time interpreted as local
                        print "Vehicle number:", vehicle_number
                        save_to_db(vehicle_number, lat_lng)
                print "Source Data:", data
            else:
                print("Data already exist in previous record.")
        except Exception as p:
            print "Something wrong!", p


# Using with Python's SocketServer
def main():
    HOST, PORT = "ipbx.dyndns.org", 9090
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
    server.socket.settimeout(10)
    server.serve_forever()


def main_old():
    sock = socket.socket()
    sock.bind(('', 9090))
    sock.listen(15)

    previous_data = None  # For tracking data from GSM tracker
    previous_date = None  # For tracking date

    print "Socket server started listening..."
    while True:
        conn, addr = sock.accept()
        print("New connection from " + addr[0])
        try:
            data = conn.recv(1024)
            current_date = datetime.datetime.today().date()
            if data != previous_data:
                print "WORKING FIRST CONDITION"
                previous_data = data
                previous_date = current_date
                # conn.send("SUCCESS\n")
                for imei in vehicle_gsm_details.iterkeys():
                    if str(imei) in data:
                        # print "We have request from Harrier GSM Tracker device."
                        imei, mcc, mnc, lac, cid = parse_request(data)
                        lat_lng = track_device(mcc, mnc, lac, cid)
                        utc_time = datetime.datetime.now(tz=tzutc)  # time in UTC
                        # local_time = utc_time.astimezone(tzlocal)  # UTC time interpreted as local
                        vehicle_number = vehicle_gsm_details.get(imei)
                        print "LAT&LNG:", lat_lng
                        print "UTC time:", utc_time
                        # print "Local time", local_time  # UTC time interpreted as local
                        print "Vehicle number:", vehicle_number
                        save_to_db(vehicle_number, lat_lng)
                print "Source Data:", data
                conn.shutdown(1)
                conn.close()
                print "Socket closed..."

            elif current_date != previous_date:
                print "WORKING SECOND CONDITION", current_date, previous_date
                print current_date == previous_date
                previous_data = data
                previous_date = current_date
                # conn.send("SUCCESS\n")
                for imei in vehicle_gsm_details.iterkeys():
                    if str(imei) in data:
                        # print "We have request from Harrier GSM Tracker device."
                        imei, mcc, mnc, lac, cid = parse_request(data)
                        lat_lng = track_device(mcc, mnc, lac, cid)
                        utc_time = datetime.datetime.now(tz=tzutc)  # time in UTC
                        # local_time = utc_time.astimezone(tzlocal)  # UTC time interpreted as local
                        vehicle_number = vehicle_gsm_details.get(imei)
                        print "LAT&LNG:", lat_lng
                        print "UTC time:", utc_time
                        # print "Local time", local_time  # UTC time interpreted as local
                        print "Vehicle number:", vehicle_number
                        save_to_db(vehicle_number, lat_lng)
                print "Source Data:", data
                conn.shutdown(1)
                conn.close()
                print "Socket closed..."

            else:
                conn.shutdown(1)
                conn.close()
                print("Data already exist in previous record.")
        except Exception as p:
            print "Something wrong!", p
            conn.shutdown(1)
            conn.close()


class Command(BaseCommand):
    def handle_noargs(self, **options):
        pass

    help = "Just run python manage.py gsm_tracker"

    def handle(self, *args, **options):
        main()

# parse_request("*HQ,353505511042795,V1,,V,,N,,E,0.00,0,,FFFFFFFF,191,309,7fd4,71#")
# print track_device(411, 777, 32724, 113)
# create_10_connections()
# print vehicle_gsm_details.get('353505511042795')
