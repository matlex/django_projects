from __future__ import unicode_literals

from django.db import models


# Create your models here.
class Coordinate(models.Model):
    geo_latitude = models.FloatField(max_length=20)
    geo_longitude = models.FloatField(max_length=20)

    class Meta:
        verbose_name = 'Coordinates'

    def __unicode__(self):
        return ', '.join([str(self.geo_latitude), str(self.geo_longitude)])


class TrackedCoordinate(models.Model):
    geo_coords = models.ForeignKey(Coordinate)
    date_and_time = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    vehicle_number = models.CharField(max_length=20, blank=False, null=False)

