from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from gsm_tracker import forms
from gsm_tracker.models import TrackedCoordinate
import datetime


# Helper Function
def get_coords(coords_list_of_objects):
    """
    Get list of coords, analyze and return without dublicates.
    If same coords goes one by one we need to remove it. For example if a car was just staying.
    """
    output = []
    last_obj_id = None
    for current_obj in coords_list_of_objects:
        if current_obj.geo_coords.id == last_obj_id:
            continue
        output.append([current_obj.geo_coords.geo_latitude, current_obj.geo_coords.geo_longitude])
        last_obj_id = current_obj.geo_coords.id
    return output


# Create your views here.
@login_required
def gsm(request):
    context = dict()
    if request.method == 'POST':
        form = forms.GsmTrackingForm(request.POST)
        if form.is_valid():
            date_to_track = form.cleaned_data['date_to_track']
            coordinates = TrackedCoordinate.objects.filter(date_and_time__year=date_to_track.year). \
                filter(date_and_time__month=date_to_track.month).filter(date_and_time__day=date_to_track.day)
            # context['geolocations'] = [[i.geo_coords.geo_latitude, i.geo_coords.geo_longitude] for i in coordinates]
            context['geolocations'] = get_coords(coordinates)  # Delete duplicates going one by one. Probka or Home:)
            if not coordinates:
                context['error_message'] = '<div class="alert alert-warning alert-dismissible" role="alert">' \
                                           '<button type="button" class="close"' \
                                           ' data-dismiss="alert" aria-label="Close">' \
                                           '<span aria-hidden="true">&times;</span></button>' \
                                           '<strong>Warning!</strong> There is no data for that date: {0}</div>'.format(
                    str(date_to_track))
                context['warning'] = 'There is no data for that date: {0}'.format(str(date_to_track))
    else:
        context['geolocations'] = []
        form = forms.GsmTrackingForm()
    context['form'] = form
    return render(request, 'gsm_tracker/gsm_tracker.html', context)
