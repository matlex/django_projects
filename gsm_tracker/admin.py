from django.contrib import admin

# Register your models here.
from gsm_tracker.models import Coordinate, TrackedCoordinate


class CoordinateAdmin(admin.ModelAdmin):
    list_display = ['geo_latitude', 'geo_longitude']
    readonly_fields = ['geo_latitude', 'geo_longitude']


class TrackedCoordinateAdmin(admin.ModelAdmin):
    list_display = ['date_and_time', 'vehicle_number', 'geo_coords']

admin.site.register(Coordinate, CoordinateAdmin)
admin.site.register(TrackedCoordinate, TrackedCoordinateAdmin)
