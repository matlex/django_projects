from django import forms
from bootstrap3_datetime.widgets import DateTimePicker
import datetime


class GsmTrackingForm(forms.Form):
    date_to_track = forms.DateField(
        widget=DateTimePicker(options={
            'format': "YYYY-MM-DD",
            'pickTime': False,
        }),

        label="Choose date",
        initial=datetime.date.today()
    )
