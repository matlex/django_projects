# -*- coding: utf-8 -*-
from django.contrib.auth.forms import AuthenticationForm
from django import forms

# Пока не юзаем это нигде...
class LoginForm(AuthenticationForm):
    username = forms.CharField(max_length=30, required=True)
    password = forms.CharField(widget=forms.PasswordInput, required=True)
