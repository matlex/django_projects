from django.shortcuts import render
from django.http import HttpResponse
import json as simplejson
import socket


def home(request):
    return render(request, 'home.html')


# Playing with Ajax just testing
def main(request):
    return render(request, 'ajaxexample.html')


# Playing with Ajax just testing
def ajax(request):
    if 'client_response' in request.POST:
        x = request.POST['client_response']
        y = socket.gethostbyname(x)
        response_dict = dict()
        response_dict['server_response'] = y
        return HttpResponse(simplejson.dumps(response_dict), content_type='application/javascript')
    else:
        return render(request, 'ajaxexample.html')
